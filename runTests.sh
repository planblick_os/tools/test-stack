#!/bin/sh
git clone --branch bdd_testsystem "https://gitlab.com/planblick_os/frontends/frontend-templates.git" "frontends"
cd "frontends" ;
git reset --hard
git pull
git reset --hard
cd ".."
docker network create kong-net
docker-compose kill
docker-compose down
docker-compose up -d --no-deps
sleep 2
docker-compose up -d
cd selenium
docker-compose up -d
cd ../frontends/tests
pip3 install -r requirements.txt --user
sleep 5
python3 -m pytest --environment live --browser_engine remote_chrome -n 3
python3 -m pytest --environment live --browser_engine remote_firefox -n 3
